'use strict';
import gsheetRoutes from './gsheet';

const routes = [...gsheetRoutes];
export default routes;
