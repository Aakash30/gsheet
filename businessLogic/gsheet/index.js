'use strict';
import Joi from 'joi';
import boom from 'boom';
import resCode from '@appconstants/responseCode.json';
import {getGSheetData, updateGSheetData} from './gsheet';

const gsheetRoutes = [{
  method: 'GET',
  path: '/gsheetdata',
  config: {
    handler: (req) => {
      const {sheetName, tableName} = req.query;
      return getGSheetData(sheetName, tableName.split(','));
    },
    plugins: {
      'hapi - swagger': {
        responses: {
          '400': {
            description: 'BadRequest'
          },
          '412': {
            'description': 'preconditionFailed'
          },
          '200': {
            description: 'ok'
          },
          '500': {
            'description': 'Internal Server error'
          }
        },
        payloadType: 'json'
      }
    },
    validate: {
      query: {
        sheetName: Joi.string().label('sheetName').required(),
        tableName: Joi.string().label('tableName').required()
      },
      failAction: async (_, __, {message}) => {
        console.error(message);
        throw boom.preconditionFailed(resCode.SCHEMA_VALIDATE);
      }
    },
    description: `Get data as response from gsheet`,
    notes: `Get data as response from gsheet`,
    tags: ['api']

  }
},
{
  method: 'POST',
  path: '/gsheetvalues',
  config: {
    handler: (req) => {
      const {sheetName, range, data} = req.payload;
      return updateGSheetData(sheetName, range, data);
    },
    plugins: {
      'hapi - swagger': {
        responses: {
          '400': {
            description: 'BadRequest'
          },
          '412': {
            'description': 'preconditionFailed'
          },
          '200': {
            description: 'ok'
          },
          '500': {
            'description': 'Internal Server error'
          }
        },
        payloadType: 'json'
      }
    },
    validate: {
      payload: {
        sheetName: Joi.string().label('sheetName').required(),
        range: Joi.string().label('range').required(),
        data: Joi.array().label('data').required()
      },
      failAction: async (_, __, {message}) => {
        console.error(message);
        throw boom.preconditionFailed(resCode.SCHEMA_VALIDATE);
      }
    },
    description: `Update values of gsheet`,
    notes: `Update values of gsheet`,
    tags: ['api']
  }
}];

export default gsheetRoutes;
