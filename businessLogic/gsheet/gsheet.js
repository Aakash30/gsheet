
'use strict';
import response from '@helpers/responseHelper';
import resCode from '@appconstants/responseCode.json';
import statusCode from '@appconstants/statusCode.json';
import boom from 'boom';
import {google} from 'googleapis';
import {get} from 'lodash';

const authorize = () => {
  console.info('authorize: Entering');
  const oAuth2Client = new google.auth.OAuth2(
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET,
    'urn:ietf:wg:oauth:2.0:oob'
  );

  oAuth2Client.setCredentials({
    'access_token': process.env.ACCESS_TOKEN,
    'refresh_token': process.env.REFRESH_TOKEN,
    'scope': 'https://www.googleapis.com/auth/spreadsheets',
    'token_type': 'Bearer',
    'expiry_date': 1582488053429
  });
  return oAuth2Client;
};

const getSheetData = async (auth, sheetName, tableName) => {
  console.info('getSheetData: Entering');
  try {
    const sheets = google.sheets({version: 'v4',
      auth});
    let responseData = await sheets.spreadsheets.values.batchGet({
      spreadsheetId: sheetName,
      ranges: tableName
    });
    responseData = get(responseData, 'data.valueRanges', null);
    let tableRows = {};
    responseData.forEach((table, idx) => {
      let rows = {};
      tableRows[tableName[idx]] = [];
      let columnNames = table.values[0];
      rows = columnNames.reduce((columns, key) => ({...columns,
        [key]: ''}), {});
      table.values.forEach((dataRows, i) => {
        if (i > 0) {
          let j = 0;
          for (let key in rows) {
            rows[key] = dataRows[j++];
          }
          tableRows[tableName[idx]].push({...rows});
        }
      });
    });
    return tableRows;
  } catch ({message}) {
    console.error('Failed to get gsheet data:', message);
    throw new Error(message);
  }
};

export const getGSheetData = async (sheetName, tableName) => {
  console.info(`GET: getGSheetData:Entering Get Google Data of ${sheetName} for tables: ${tableName}`);
  try {
    const auth = authorize();
    const gSheetData = await getSheetData(auth, sheetName, tableName);
    return response.successHandler(statusCode.OK, resCode.OK, gSheetData);
  } catch ({message}) {
    console.error(`Get:getGSheetData:error: ${message}`);
    return boom.badImplementation(message);
  }
};

const updateSheetData = async (sheetName, range, data, auth) => {
  console.info('updateSheetData: Entering');
  try {
    const sheets = google.sheets({version: 'v4',
      auth});
    let responseData = await sheets.spreadsheets.values.append({
      spreadsheetId: sheetName,
      range: range,
      valueInputOption: 'RAW',
      resource: {
        values: data
      }
    });
    return JSON.parse(JSON.stringify(responseData.data, null, 2));
  } catch ({message}) {
    console.error('Failed to update sheet:', message);
    throw new Error(message);
  }
};

export const updateGSheetData = async (sheetName, range, data) => {
  console.info('POST: updateGSheetData: Entering: update Google Data of', sheetName,
    'with', range, 'for', data);
  try {
    const auth = authorize();
    const updatedSheetData = await updateSheetData(sheetName, range, data, auth);
    return response.successHandler(statusCode.OK, resCode.OK, updatedSheetData);
  } catch ({message}) {
    console.error(`POST:updateGSheetData:error: ${message}`);
    return boom.badImplementation(message); ;
  }
};
