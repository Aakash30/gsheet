'use strict';
import demoSchedule from './demoSchedule';

const handlers = {
  ...demoSchedule
};
export default handlers;
