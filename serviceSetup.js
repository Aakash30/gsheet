module.exports = {
  name: 'gsheet',
  version: 'v1',
  description: `this service will get data from google sheet for multiple 
                tables and also it will update the data to googlesheet`,
  config: {
    isDBRequired: true,
    isQueueRequired: false,
    isCachingRequired: false,
    isJobRequired: false,
    queue: {
      topicsToPublish: [],
      topicsToDelete: [],
      subscripton: [],
      unsubcribe: []
    },
    chachingKey: 'gsheet',
    jobs: []
  }
};
